
use student;

select * from course;
select * from sc;
select * from student;
select * from teacher;

-- 14.查询各科成绩最高分、最低分和平均分,以如下形式显示：
-- 课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
-- 及格为>=60，中等为：70-80，优良为：[80-90)，优秀为：>=90
-- 要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序

select a.cid 课程ID,b.cname 课程名称, max(score) 最高分,min(score) 最低分, avg(score) 平均分,sum(IF(score>=60,1,0))/count(*) 及格率,sum(if(score>=70&&score<80,1,0))/count(*) 中等率,sum(if(score>=80&&score<90,1,0))/count(*) 优良率 ,sum(if(score>=90,1,0))/count(*) 优秀率 from sc a inner join course b on a.cid=b.cid group by a.cid;

-- 15.按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺

select *,(select count(*)+1 from sc a where a.cid=b.cid and a.score>b.score) 排名 from sc b ORDER BY b.cid asc,排名 asc;

-- 15.1 按各科成绩进行行排序，并显示排名， Score 重复时合并名次

select *,(select count(DISTINCT a.score)+1 from sc a where a.cid=sc.cid and a.score>sc.score) 排名 from sc order by sc.cid asc,排名 asc;

-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺

select *,(
select count(a.`总成绩`)+1 from 
(
	select sid, sum(score) 总成绩 from sc group by sid
)a where a.`总成绩`>b.`总成绩`
) 排名 from (select sid, sum(score) 总成绩 from sc group by sid) b order by 排名 asc;

-- 17. 统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比

select b.cname, a.*,sum(if(score>=85&&score<100,1,0))/count(*) `[100-85]`, sum(if(score>=70&&score<85,1,0))/count(*) `[85-70]`,sum(if(score>=60&&score<70,1,0))/count(*) `[70-60]`,sum(if(score>=0&&score<60,1,0))/count(*) `[60-0]` from sc a inner join course b on a.cid=b.cid group by cid;

-- 18.查询各科成绩前三名的记录

select * from (
select *,(select count(distinct a.score)+1 from sc a where a.cid=b.cid and a.score>b.score) 排名 from sc b order by cid asc,排名 asc)
c where 排名<=3;

-- 19.查询每门课程被选修的学生数

select *,count(*) from sc a inner join course b on a.cid=b.cid group by a.cid;


-- 20.查询出只选修两门课程的学生学号和姓名

select * from student where sid in 
(
select sid from sc group by sid HAVING count(*)=2
);

-- 21. 查询男生、女生人数

select 
(select count(*) from student where ssex='男') 男生人数,
(select count(*) from student where ssex='女') 女生人数;

-- 22. 查询名字中含有「风」字的学生信息

select * from student where sname like '%风%';

-- 23查询同名同姓学生名单，并统计同名人数

select * ,count(*) from student GROUP BY SNAME HAVING count(*)>1;

-- 24.查询 1990 年出生的学生名单

SELECT * from student where sage like '1990%';

-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列

select *,avg(score) 平均成绩 from sc group by cid order by 平均成绩 desc,cid asc;

-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩

select * from(
select *,avg(score)平均成绩  from sc GROUP BY sid HAVING 平均成绩>=85)a inner join student b on a.sid=b.sid;
