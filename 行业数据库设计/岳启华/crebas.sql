/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/7 17:18:17                            */
/*==============================================================*/


drop table if exists Classroom_Equip;

drop table if exists Department;

drop table if exists Department_Student_Num;

drop table if exists Department_Student_Score;

drop table if exists Dormitory_info;

drop table if exists Organization;

drop table if exists Prefessional_Course_info;

drop table if exists Professional_Teacher_info;

drop table if exists Rcent_graduate;

drop index Index_1 on Schol&Firm_table;

drop table if exists Schol&Firm_table;

drop table if exists Student_Course_Table;

drop index Index_2 on Student_info;

drop index Index_1 on Student_info;

drop table if exists Student_info;

drop index Index_1 on Teacher_info;

drop table if exists Teacher_info;

drop table if exists complain_info;

drop table if exists profession_info;

/*==============================================================*/
/* Table: Classroom_Equip                                       */
/*==============================================================*/
create table Classroom_Equip
(
   id                   int not null auto_increment,
   profession_room      varchar(10),
   equip_info           text,
   lend_info            enum(0,1),
   primary key (id)
);

/*==============================================================*/
/* Table: Department                                            */
/*==============================================================*/
create table Department
(
   id                   int not null auto_increment,
   department_name      varchar(10),
   dean                 varchar(10),
   info                 text,
   primary key (id)
);

/*==============================================================*/
/* Table: Department_Student_Num                                */
/*==============================================================*/
create table Department_Student_Num
(
   id                   int not null,
   department_id        int,
   profession_id        int,
   num_of_people        int,
   class_num            int,
   primary key (id)
);

/*==============================================================*/
/* Table: Department_Student_Score                              */
/*==============================================================*/
create table Department_Student_Score
(
   id                   int not null auto_increment,
   student_id           int,
   if_fail              enum(0,1),
   primary key (id)
);

/*==============================================================*/
/* Table: Dormitory_info                                        */
/*==============================================================*/
create table Dormitory_info
(
   id                   int not null,
   Dormitory_name       varchar(10),
   Dormitory_equip      varchar(20),
   Department_id        int,
   Dormitory_num        int,
   primary key (id)
);

/*==============================================================*/
/* Table: Organization                                          */
/*==============================================================*/
create table Organization
(
   id                   int not null auto_increment,
   or_name              varchar(10),
   info                 text,
   number_of_people     int,
   primary key (id)
);

/*==============================================================*/
/* Table: Prefessional_Course_info                              */
/*==============================================================*/
create table Prefessional_Course_info
(
   course_id            int not null auto_increment,
   department_id        int,
   course_allnum        int,
   primary key (course_id)
);

/*==============================================================*/
/* Table: Professional_Teacher_info                             */
/*==============================================================*/
create table Professional_Teacher_info
(
   id                   int not null auto_increment,
   course_id            int,
   teacher_id           int,
   primary key (id)
);

/*==============================================================*/
/* Table: Rcent_graduate                                        */
/*==============================================================*/
create table Rcent_graduate
(
   id                   int not null auto_increment,
   year                 year(4),
   recent_num           int,
   work_percent         decimal(1,2),
   primary key (id)
);

/*==============================================================*/
/* Table: Schol&Firm_table                                      */
/*==============================================================*/
create table Schol&Firm_table
(
   id                   int not null auto_increment,
   name                 varchar(10),
   S&F_info             text,
   city                 varchar(20),
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on Schol&Firm_table
(
   S&F_info
);

/*==============================================================*/
/* Table: Student_Course_Table                                  */
/*==============================================================*/
create table Student_Course_Table
(
   id                   int not null,
   course_id            int,
   student_id           int,
   primary key (id)
);

/*==============================================================*/
/* Table: Student_info                                          */
/*==============================================================*/
create table Student_info
(
   id                   int not null,
   name                 varchar(10),
   department_id        int,
   profession_id        int,
   num                  varchar(10),
   age                  int,
   high                 float,
   weight               float,
   sex                  tinyint,
   born_date            datetime,
   idcrad               varchar(18),
   place                text,
   bankcard             varchar(21),
   povety               tinyint,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on Student_info
(
   idcrad
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create unique index Index_2 on Student_info
(
   bankcard
);

/*==============================================================*/
/* Table: Teacher_info                                          */
/*==============================================================*/
create table Teacher_info
(
   id                   int not null auto_increment,
   name                 varchar(10),
   age                  int,
   num                  varchar(10),
   professor_to         char(10),
   work_age             int,
   evaluate_info        enum(0,1),
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on Teacher_info
(
   num
);

/*==============================================================*/
/* Table: complain_info                                         */
/*==============================================================*/
create table complain_info
(
   id                   int not null,
   dormitory_id         int,
   place_info           text,
   complain_info        text,
   if_slove             enum(0,1),
   primary key (id)
);

/*==============================================================*/
/* Table: profession_info                                       */
/*==============================================================*/
create table profession_info
(
   profession_code      varchar(9) not null,
   profession_name      varchar(10),
   department_id        char(10),
   enroll_num           int,
   enroll_minscore      int,
   charge               decimal(5,2),
   primary key (profession_code)
);

alter table Department_Student_Num add constraint FK_Reference_9 foreign key (department_id)
      references Department (id) on delete restrict on update restrict;

alter table Department_Student_Score add constraint FK_Reference_7 foreign key (student_id)
      references Student_info (id) on delete restrict on update restrict;

alter table Dormitory_info add constraint FK_Reference_12 foreign key (Department_id)
      references Department (id) on delete restrict on update restrict;

alter table Prefessional_Course_info add constraint FK_Reference_11 foreign key (department_id)
      references Department (id) on delete restrict on update restrict;

alter table Professional_Teacher_info add constraint FK_Reference_5 foreign key (teacher_id)
      references Teacher_info (id) on delete restrict on update restrict;

alter table Professional_Teacher_info add constraint FK_Reference_6 foreign key (course_id)
      references Prefessional_Course_info (course_id) on delete restrict on update restrict;

alter table Student_Course_Table add constraint FK_Reference_3 foreign key (student_id)
      references Student_info (id) on delete restrict on update restrict;

alter table Student_Course_Table add constraint FK_Reference_4 foreign key (course_id)
      references Prefessional_Course_info (course_id) on delete restrict on update restrict;

alter table Student_info add constraint FK_Reference_1 foreign key (profession_id)
      references profession_info (profession_code) on delete restrict on update restrict;

alter table Student_info add constraint FK_Reference_10 foreign key (department_id)
      references Department (id) on delete restrict on update restrict;

alter table Teacher_info add constraint FK_Reference_2 foreign key (professor_to)
      references profession_info (profession_code) on delete restrict on update restrict;

alter table complain_info add constraint FK_Reference_8 foreign key (dormitory_id)
      references Dormitory_info (id) on delete restrict on update restrict;

