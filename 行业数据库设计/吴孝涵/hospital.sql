/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : localhost:3306
 Source Schema         : hospital

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 04/09/2021 11:56:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for check_order
-- ----------------------------
DROP TABLE IF EXISTS `check_order`;
CREATE TABLE `check_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `result` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结果',
  `master_id` int(11) NULL DEFAULT NULL COMMENT '创建人员ID',
  `cheker_id` int(11) NULL DEFAULT NULL COMMENT '检测人员ID',
  `sick_person_id` int(11) NULL DEFAULT NULL COMMENT '患者ID',
  `machine_id` int(11) NULL DEFAULT NULL COMMENT '仪器ID',
  `see_doctor_order_id` int(11) NULL DEFAULT NULL COMMENT '门诊订单ID',
  `count` decimal(24, 6) NULL DEFAULT NULL COMMENT '次数',
  `price` decimal(24, 6) NULL DEFAULT NULL COMMENT '金额',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `sent_time` datetime(0) NULL DEFAULT NULL COMMENT '完成时间',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `check_machine`(`machine_id`) USING BTREE,
  INDEX `check_sick`(`sick_person_id`) USING BTREE,
  INDEX `check_see`(`see_doctor_order_id`) USING BTREE,
  INDEX `check_woker`(`cheker_id`) USING BTREE,
  INDEX `check_creater`(`master_id`) USING BTREE,
  CONSTRAINT `check_machine` FOREIGN KEY (`machine_id`) REFERENCES `machine` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `check_see` FOREIGN KEY (`see_doctor_order_id`) REFERENCES `see_doctor_order` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `check_sick` FOREIGN KEY (`sick_person_id`) REFERENCES `see_doctor_order` (`sick_person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `check_woker` FOREIGN KEY (`cheker_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `check_creater` FOREIGN KEY (`master_id`) REFERENCES `see_doctor_order` (`doctor_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '检测订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of check_order
-- ----------------------------
INSERT INTO `check_order` VALUES (1, '指甲确实很长', 2, 5, 7, 2, 1, 1.000000, 200.000000, '2021-09-03 22:52:09', '2021-09-04 09:49:03', 1, 1);

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `master_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主管ID',
  `parent_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上级ID',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES (1, '门诊部', NULL, NULL, '2021-09-03 20:39:21', 1);
INSERT INTO `department` VALUES (2, '住院部', NULL, NULL, '2021-09-03 20:39:33', 1);
INSERT INTO `department` VALUES (3, '药房', NULL, NULL, '2021-09-03 20:39:41', 1);
INSERT INTO `department` VALUES (4, '中药房', NULL, '3', '2021-09-03 20:39:54', 1);
INSERT INTO `department` VALUES (5, '西药房', NULL, '3', '2021-09-03 20:39:58', 1);
INSERT INTO `department` VALUES (6, '检测科', NULL, NULL, '2021-09-03 20:40:10', 1);
INSERT INTO `department` VALUES (7, '影像科', NULL, NULL, '2021-09-03 20:40:19', 1);
INSERT INTO `department` VALUES (8, '急诊部', NULL, '1', '2021-09-03 20:40:49', 1);

-- ----------------------------
-- Table structure for department_room
-- ----------------------------
DROP TABLE IF EXISTS `department_room`;
CREATE TABLE `department_room`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `department_id` int(11) NULL DEFAULT NULL COMMENT '部门ID',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '上级科室ID',
  `master_id` int(11) NULL DEFAULT NULL COMMENT '主管ID',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `dept_room_woker`(`master_id`) USING BTREE,
  INDEX `dept_room_dept`(`department_id`) USING BTREE,
  CONSTRAINT `dept_room_woker` FOREIGN KEY (`master_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dept_room_dept` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '科室信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department_room
-- ----------------------------
INSERT INTO `department_room` VALUES (1, '外科', 1, NULL, NULL, 1);
INSERT INTO `department_room` VALUES (2, '普通外科', 1, 1, 4, 1);
INSERT INTO `department_room` VALUES (5, '外科手术室', 1, 1, 4, 1);
INSERT INTO `department_room` VALUES (6, '住院部', 2, NULL, NULL, 1);
INSERT INTO `department_room` VALUES (7, '住院部1楼', 2, 6, 1, 1);
INSERT INTO `department_room` VALUES (8, '住院部1楼101', 2, 6, 1, 1);

-- ----------------------------
-- Table structure for drug
-- ----------------------------
DROP TABLE IF EXISTS `drug`;
CREATE TABLE `drug`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品名',
  `brand` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌',
  `count` decimal(24, 6) NULL DEFAULT NULL COMMENT '数量',
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `recive_time` datetime(0) NULL DEFAULT NULL COMMENT '入库时间',
  `past_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `recive_master_id` int(11) NULL DEFAULT NULL COMMENT '接收人ID',
  `sup_price` decimal(24, 6) NULL DEFAULT NULL COMMENT '供应价格',
  `sup_company` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `durg_woker`(`recive_master_id`) USING BTREE,
  CONSTRAINT `durg_woker` FOREIGN KEY (`recive_master_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '药品信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of drug
-- ----------------------------
INSERT INTO `drug` VALUES (1, '维生素C片', '奥里给', 1000.000000, '1', '2021-09-03 23:10:01', '2022-09-01 23:10:03', 3, 100.000000, '院长表哥', 1);
INSERT INTO `drug` VALUES (2, '维生素C1片', '奥利给', 1000.000000, '3', '2021-09-03 23:10:01', '2022-09-01 23:10:03', 3, 100.000000, '院长表哥', 1);
INSERT INTO `drug` VALUES (3, '维生素C片', '奥奥里给', 998.000000, '2', '2021-09-04 23:10:01', '2022-09-01 23:10:03', 3, 100.000000, '院长表哥', 1);
INSERT INTO `drug` VALUES (4, '维生素C1片', 'aoligei', 1000.000000, '1', '2021-09-04 23:10:01', '2022-09-01 23:10:03', 3, 100.000000, '院长表哥', 1);
INSERT INTO `drug` VALUES (5, '维生素C片', '奥里给', 152.000000, '1', '2021-09-03 23:10:01', '2022-09-01 23:10:03', 3, 100.000000, '院长表哥', 1);

-- ----------------------------
-- Table structure for drug_order
-- ----------------------------
DROP TABLE IF EXISTS `drug_order`;
CREATE TABLE `drug_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `see_doctor_order_id` int(11) NULL DEFAULT NULL COMMENT '门诊订单ID',
  `drug_id` int(11) NULL DEFAULT NULL COMMENT '药品ID',
  `usage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用法',
  `dose` decimal(24, 6) NULL DEFAULT NULL COMMENT '剂量',
  `price` decimal(24, 6) NULL DEFAULT NULL COMMENT '金额',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `drug_order_drug`(`drug_id`) USING BTREE,
  INDEX `drug_order_see`(`see_doctor_order_id`) USING BTREE,
  CONSTRAINT `drug_order_drug` FOREIGN KEY (`drug_id`) REFERENCES `drug` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `drug_order_see` FOREIGN KEY (`see_doctor_order_id`) REFERENCES `see_doctor_order` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '药品订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of drug_order
-- ----------------------------
INSERT INTO `drug_order` VALUES (1, 1, 1, '一次30片，一日三次', 500.000000, 500.000000, '2021-09-03 23:24:45', 1);

-- ----------------------------
-- Table structure for machine
-- ----------------------------
DROP TABLE IF EXISTS `machine`;
CREATE TABLE `machine`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `type` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  `sup_company` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '供应商',
  `sup_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '供应价格',
  `recive_time` datetime(0) NULL DEFAULT NULL COMMENT '接收时间',
  `department_room_id` int(11) NULL DEFAULT NULL COMMENT '部门科室ID',
  `master_id` int(11) NULL DEFAULT NULL COMMENT '主管ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `machine_woker`(`master_id`) USING BTREE,
  INDEX `department_room_id`(`department_room_id`) USING BTREE,
  CONSTRAINT `machine_woker` FOREIGN KEY (`master_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `machine_ibfk_1` FOREIGN KEY (`department_room_id`) REFERENCES `department_room` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '器械信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of machine
-- ----------------------------
INSERT INTO `machine` VALUES (1, '救护车', '3', 1, '院长表弟', 500000.00, '2021-09-03 22:43:31', 1, 5);
INSERT INTO `machine` VALUES (2, '指甲剪', '1', 1, '院长表弟', 1000.00, '2021-09-03 22:46:10', 5, 1);
INSERT INTO `machine` VALUES (3, '大指甲剪', '1', 0, '院长表弟', 2000.00, NULL, 5, 1);
INSERT INTO `machine` VALUES (4, '小指甲剪', '1', 0, '院长表弟', 2000.00, NULL, 5, 1);

-- ----------------------------
-- Table structure for operation_order
-- ----------------------------
DROP TABLE IF EXISTS `operation_order`;
CREATE TABLE `operation_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手术名称',
  `price` decimal(24, 6) NULL DEFAULT NULL COMMENT '价格',
  `order_time` datetime(0) NULL DEFAULT NULL COMMENT '预定时间',
  `sick_person_id` int(11) NULL DEFAULT NULL COMMENT '患者ID',
  `master_id` int(11) NULL DEFAULT NULL COMMENT '医生ID',
  `department_room_id` int(11) NULL DEFAULT NULL COMMENT '手术室ID',
  `see_doctor_order_id` int(11) NULL DEFAULT NULL COMMENT '诊断订单ID',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oper_see`(`see_doctor_order_id`) USING BTREE,
  INDEX `oper_room`(`department_room_id`) USING BTREE,
  INDEX `oper_woker`(`master_id`) USING BTREE,
  INDEX `oper_sick_person`(`sick_person_id`) USING BTREE,
  CONSTRAINT `oper_room` FOREIGN KEY (`department_room_id`) REFERENCES `department_room` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `oper_see` FOREIGN KEY (`see_doctor_order_id`) REFERENCES `see_doctor_order` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `oper_sick_person` FOREIGN KEY (`sick_person_id`) REFERENCES `sick_person` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `oper_woker` FOREIGN KEY (`master_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '手术订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of operation_order
-- ----------------------------
INSERT INTO `operation_order` VALUES (1, '剪指甲', 100.000000, '2021-09-05 09:16:33', 7, 2, 5, 1, 1);

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `department_room_id` int(11) NULL DEFAULT NULL COMMENT '诊室ID',
  `sick_person_id` int(11) NULL DEFAULT NULL COMMENT '就诊人ID',
  `order_date` date NULL DEFAULT NULL COMMENT '预约日期',
  `order_time` time(0) NULL DEFAULT NULL COMMENT '预约时间',
  `price` decimal(24, 6) NULL DEFAULT NULL COMMENT '挂号费用',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `order_dept_room`(`department_room_id`) USING BTREE,
  INDEX `order_person`(`sick_person_id`) USING BTREE,
  CONSTRAINT `order_dept_room` FOREIGN KEY (`department_room_id`) REFERENCES `department_room` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `order_person` FOREIGN KEY (`sick_person_id`) REFERENCES `sick_person` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '预约信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES (1, 2, 3, '2021-09-17', '21:43:35', 16.100000, 2);
INSERT INTO `order` VALUES (2, 2, 4, '2021-09-03', '21:33:17', 10.000000, 2);
INSERT INTO `order` VALUES (3, 2, 2, '2021-09-03', '21:43:35', 10.000000, 2);
INSERT INTO `order` VALUES (4, 2, 7, '2021-09-03', '21:10:17', 10.000000, 2);

-- ----------------------------
-- Table structure for see_doctor_order
-- ----------------------------
DROP TABLE IF EXISTS `see_doctor_order`;
CREATE TABLE `see_doctor_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `result` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '诊断结果',
  `doctor_id` int(11) NULL DEFAULT NULL COMMENT '医生ID',
  `sick_person_id` int(11) NULL DEFAULT NULL COMMENT '患者ID',
  `department_room_id` int(11) NULL DEFAULT NULL COMMENT '科室ID',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `see_woker`(`doctor_id`) USING BTREE,
  INDEX `see_person`(`sick_person_id`) USING BTREE,
  INDEX `see_dept_room`(`department_room_id`) USING BTREE,
  CONSTRAINT `see_dept_room` FOREIGN KEY (`department_room_id`) REFERENCES `department_room` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `see_person` FOREIGN KEY (`sick_person_id`) REFERENCES `sick_person` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `see_woker` FOREIGN KEY (`doctor_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '就诊信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of see_doctor_order
-- ----------------------------
INSERT INTO `see_doctor_order` VALUES (1, '指甲太长', 2, 7, 2, '2021-09-03 22:49:33', 2, 1);
INSERT INTO `see_doctor_order` VALUES (2, '头发太长', 2, 6, 2, '2021-09-03 10:09:58', 3, 1);
INSERT INTO `see_doctor_order` VALUES (3, '头发太长1', 2, 6, 2, '2021-09-03 10:09:58', 3, 1);
INSERT INTO `see_doctor_order` VALUES (4, '头发太长1', 2, 6, 2, '2021-09-04 10:09:58', 3, 1);

-- ----------------------------
-- Table structure for sick_bed
-- ----------------------------
DROP TABLE IF EXISTS `sick_bed`;
CREATE TABLE `sick_bed`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `bed_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '病床号',
  `master_id` int(11) NULL DEFAULT NULL COMMENT '管理员ID',
  `department_room_id` int(11) NULL DEFAULT NULL COMMENT '所属病房ID',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sick_bed_room`(`department_room_id`) USING BTREE,
  INDEX `sick_bed_woker`(`master_id`) USING BTREE,
  CONSTRAINT `sick_bed_room` FOREIGN KEY (`department_room_id`) REFERENCES `department_room` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `sick_bed_woker` FOREIGN KEY (`master_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '病床信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sick_bed
-- ----------------------------
INSERT INTO `sick_bed` VALUES (1, 1, '001', 4, 8, 1);
INSERT INTO `sick_bed` VALUES (2, 1, '002', 4, 5, 1);
INSERT INTO `sick_bed` VALUES (3, 1, '002', 4, 8, 1);

-- ----------------------------
-- Table structure for sick_bed_order
-- ----------------------------
DROP TABLE IF EXISTS `sick_bed_order`;
CREATE TABLE `sick_bed_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '费用',
  `see_doctor_order_id` int(11) NULL DEFAULT NULL COMMENT '门诊订单ID',
  `sick_bed_id` int(11) NULL DEFAULT NULL COMMENT '病床ID',
  `recive_time` datetime(0) NULL DEFAULT NULL COMMENT '接收病人时间',
  `sent_time` datetime(0) NULL DEFAULT NULL COMMENT '病人出院时间',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `bed_order_bed`(`sick_bed_id`) USING BTREE,
  INDEX `bed_order_see`(`see_doctor_order_id`) USING BTREE,
  CONSTRAINT `bed_order_bed` FOREIGN KEY (`sick_bed_id`) REFERENCES `sick_bed` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bed_order_see` FOREIGN KEY (`see_doctor_order_id`) REFERENCES `see_doctor_order` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '病床信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sick_bed_order
-- ----------------------------
INSERT INTO `sick_bed_order` VALUES (1, 1, 30.00, 1, 1, '2021-09-04 08:48:54', '2021-09-07 09:09:21', 2);

-- ----------------------------
-- Table structure for sick_person
-- ----------------------------
DROP TABLE IF EXISTS `sick_person`;
CREATE TABLE `sick_person`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `card_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '就诊卡号',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '生日',
  `gender` int(11) NULL DEFAULT NULL COMMENT '性别',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '患者信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sick_person
-- ----------------------------
INSERT INTO `sick_person` VALUES (1, '001', 'zhangsan', '2021-09-03 20:45:21', 1, 1, 1);
INSERT INTO `sick_person` VALUES (2, '002', 'zhangan1', '2021-09-03 20:45:21', 1, 1, 1);
INSERT INTO `sick_person` VALUES (3, '003', 'zhangsan1', '2021-09-03 20:45:21', 1, 1, 1);
INSERT INTO `sick_person` VALUES (4, '004', 'zhangsan1', '2021-09-03 20:45:21', 0, 1, 1);
INSERT INTO `sick_person` VALUES (5, '005', 'zhangsan2', '2021-09-03 20:45:21', 1, 1, 1);
INSERT INTO `sick_person` VALUES (6, '006', 'zhangsan3', '2021-09-03 20:45:21', 0, 1, 1);
INSERT INTO `sick_person` VALUES (7, '007', 'zhangsan4', '2021-09-03 20:45:21', 0, 1, 1);
INSERT INTO `sick_person` VALUES (8, '008', 'zhangsan5', '2021-09-03 20:45:21', 1, 1, 1);
INSERT INTO `sick_person` VALUES (9, '009', 'lisi', '2021-09-03 20:45:21', 0, 1, 1);
INSERT INTO `sick_person` VALUES (10, '010', 'lisi1', '2021-09-03 20:45:21', 1, 1, 1);
INSERT INTO `sick_person` VALUES (11, '011', 'lisi12', '2021-09-03 20:45:21', 1, 1, 1);
INSERT INTO `sick_person` VALUES (12, '012', 'lisi123', '2021-09-03 20:45:21', 0, 1, 1);
INSERT INTO `sick_person` VALUES (13, '013', 'lisi1235', '2021-09-03 20:45:21', 1, 1, 1);

-- ----------------------------
-- Table structure for woker
-- ----------------------------
DROP TABLE IF EXISTS `woker`;
CREATE TABLE `woker`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `gender` int(11) NULL DEFAULT NULL COMMENT '性别',
  `department_id` int(11) NULL DEFAULT NULL COMMENT '部门ID',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `woker_dept`(`department_id`) USING BTREE,
  CONSTRAINT `woker_dept` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工作人员信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of woker
-- ----------------------------
INSERT INTO `woker` VALUES (1, 'zhangsan', 23, 0, 1, 1);
INSERT INTO `woker` VALUES (2, 'zhangsan1', 24, 1, 1, 1);
INSERT INTO `woker` VALUES (3, 'zhangsan2', 25, 0, 1, 1);
INSERT INTO `woker` VALUES (4, 'lisi', 26, 1, 1, 1);
INSERT INTO `woker` VALUES (5, '救护车司机', 27, 1, 8, 1);

SET FOREIGN_KEY_CHECKS = 1;
