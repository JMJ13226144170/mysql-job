/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/7 17:10:05                            */
/*==============================================================*/


drop table if exists goods;

drop table if exists merchant;

drop table if exists "order";

drop table if exists rider;

drop table if exists user;

/*==============================================================*/
/* Table: goods                                                 */
/*==============================================================*/
create table goods
(
   goods_id             int not null auto_increment,
   merchant_id          int not null,
   goods_name           varchae(30) not null,
   sales_volume         char(10) not null,
   primary key (goods_id)
);

/*==============================================================*/
/* Table: merchant                                              */
/*==============================================================*/
create table merchant
(
   merchant_id          int not null auto_increment,
   merchant_name        varchar(30) not null,
   merchant_address     varchar(30) not null,
   merchant_type        varchar(30) not null,
   primary key (merchant_id)
);

/*==============================================================*/
/* Table: "order"                                               */
/*==============================================================*/
create table "order"
(
   order_id             int not null auto_increment,
   user_evaluation      varchar(30) not null,
   order_status         varchar(80) not null,
   merchant_id          int not null,
   user_id              int not null,
   rider_id             int not null,
   primary key (order_id)
);

/*==============================================================*/
/* Table: rider                                                 */
/*==============================================================*/
create table rider
(
   rider_id             int not null auto_increment,
   rider_name           varchar(30) not null,
   rider_credit         enum(2) not null,
   rider_health         enum(2),
   rider_num            char(11),
   primary key (rider_id)
);

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
create table user
(
   user_id              int not null auto_increment,
   user_address         varchar(50),
   user_name            varchar(20) not null,
   user_num             char(11) not null,
   primary key (user_id)
);

alter table goods add constraint FK_Reference_4 foreign key (merchant_id)
      references merchant (merchant_id) on delete restrict on update restrict;

alter table "order" add constraint FK_Reference_1 foreign key (merchant_id)
      references merchant (merchant_id) on delete restrict on update restrict;

alter table "order" add constraint FK_Reference_2 foreign key (user_id)
      references user (user_id) on delete restrict on update restrict;

alter table "order" add constraint FK_Reference_3 foreign key (rider_id)
      references rider (rider_id) on delete restrict on update restrict;

