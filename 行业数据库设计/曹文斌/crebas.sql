/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/7 17:34:05                            */
/*==============================================================*/


drop table if exists jspbb;

drop table if exists ssjcxxb;

drop index Index_1 on studentxuanke;

drop table if exists studentxuanke;

drop index Index_4 on studnet;

drop index sfz on studnet;

drop index Index_2 on studnet;

drop index Index_1 on studnet;

drop table if exists studnet;

drop table if exists txfkb;

drop table if exists xqhzb;

drop table if exists xyxscjb;

drop table if exists yuanxibiao;

drop table if exists yxxsrsb;

drop index Index_1 on zdlsb;

drop table if exists zdlsb;

drop table if exists zyxxb;

drop table if exists 专业课信息表;

/*==============================================================*/
/* Table: jspbb                                                 */
/*==============================================================*/
create table jspbb
(
   id                   int not null auto_increment,
   zyjs                 varchar(10),
   pbqk                 varchar(60),
   syqk                 tinyint default 1,
   primary key (id)
);

/*==============================================================*/
/* Table: ssjcxxb                                               */
/*==============================================================*/
create table ssjcxxb
(
   id                   int not null,
   sushe_name           varchar(10),
   sushe_pebe           varchar(60),
   ssyx                 int,
   ssrs                 int,
   primary key (id)
);

/*==============================================================*/
/* Table: studentxuanke                                         */
/*==============================================================*/
create table studentxuanke
(
   id                   int not null,
   kecheng_id           int,
   sutdent_id           int,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on studentxuanke
(
   id
);

/*==============================================================*/
/* Table: studnet                                               */
/*==============================================================*/
create table studnet
(
   id                   int not null auto_increment,
   name                 varchar(10) not null,
   yxid                 int,
   zhuanye              int,
   xuehao               varchar(10),
   age                  int,
   height               float,
   weight               float,
   gender               tinyint,
   date                 datetime,
   sfz                  varchar(18),
   "add"                varchar(50),
   yhkh                 varchar(21),
   sfspks               tinyint default 0,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on studnet
(
   xuehao
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create unique index Index_2 on studnet
(
   sfz
);

/*==============================================================*/
/* Index: sfz                                                   */
/*==============================================================*/
create unique index sfz on studnet
(
   sfz
);

/*==============================================================*/
/* Index: Index_4                                               */
/*==============================================================*/
create unique index Index_4 on studnet
(
   yhkh
);

/*==============================================================*/
/* Table: txfkb                                                 */
/*==============================================================*/
create table txfkb
(
   id                   int not null auto_increment,
   宿舍id                 int,
   xxdz                 varchar(20),
   tsxq                 varchar(60),
   sfdyjj               tinyint default 1,
   primary key (id)
);

/*==============================================================*/
/* Table: xqhzb                                                 */
/*==============================================================*/
create table xqhzb
(
   id                   int not null auto_increment,
   name                 varchar(10) not null,
   xqxx                 text,
   city                 varchar(30),
   primary key (id)
);

/*==============================================================*/
/* Table: xyxscjb                                               */
/*==============================================================*/
create table xyxscjb
(
   id                   int not null auto_increment,
   学生id                 int,
   sfczgk               tinyint default 1,
   primary key (id)
);

/*==============================================================*/
/* Table: yuanxibiao                                            */
/*==============================================================*/
create table yuanxibiao
(
   id                   int not null auto_increment,
   yx_name              varchar(10),
   yz                   varchar(10),
   xqxx                 varchar(60),
   primary key (id)
);

/*==============================================================*/
/* Table: yxxsrsb                                               */
/*==============================================================*/
create table yxxsrsb
(
   id                   int not null,
   yuanxi_id            int,
   zhuanye_id           int,
   renshu               int,
   bjgs                 int,
   primary key (id)
);

/*==============================================================*/
/* Table: zdlsb                                                 */
/*==============================================================*/
create table zdlsb
(
   id                   int not null auto_increment,
   name                 varchar(10) not null,
   gonghao              varchar(10) not null,
   age                  int not null,
   sdrzy                int,
   workage              int,
   pdqk                 tinyint default 1,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on zdlsb
(
   gonghao
);

/*==============================================================*/
/* Table: zyxxb                                                 */
/*==============================================================*/
create table zyxxb
(
   zybh                 varchar(9) not null,
   zyname               varchar(10),
   yxID                 int,
   zsrs                 int,
   zsfsx                int,
   sfje                 decimal(10,2),
   primary key (zybh)
);

/*==============================================================*/
/* Table: 专业课信息表                                                */
/*==============================================================*/
create table 专业课信息表
(
   kecheng_id           int not null,
   yuanxi_id            int,
   kczme                int,
   primary key (kecheng_id)
);

alter table ssjcxxb add constraint FK_Reference_11 foreign key (id)
      references yuanxibiao (id) on delete restrict on update restrict;

alter table ssjcxxb add constraint FK_Reference_12 foreign key (id)
      references txfkb (id) on delete restrict on update restrict;

alter table studentxuanke add constraint FK_Reference_6 foreign key (id)
      references studnet (id) on delete restrict on update restrict;

alter table studnet add constraint FK_Reference_2 foreign key (yxid)
      references yuanxibiao (id) on delete restrict on update restrict;

alter table studnet add constraint FK_Reference_3 foreign key (zhuanye)
      references zyxxb (zybh) on delete restrict on update restrict;

alter table yxxsrsb add constraint FK_Reference_10 foreign key (id)
      references zyxxb (zybh) on delete restrict on update restrict;

alter table yxxsrsb add constraint FK_Reference_9 foreign key (yuanxi_id)
      references yuanxibiao (id) on delete restrict on update restrict;

alter table zdlsb add constraint FK_Reference_4 foreign key (sdrzy)
      references zyxxb (zybh) on delete restrict on update restrict;

alter table zyxxb add constraint FK_Reference_1 foreign key (yxID)
      references yuanxibiao (id) on delete restrict on update restrict;

alter table 专业课信息表 add constraint FK_Reference_7 foreign key (kecheng_id)
      references studentxuanke (id) on delete restrict on update restrict;

alter table 专业课信息表 add constraint FK_Reference_8 foreign key (yuanxi_id)
      references yuanxibiao (id) on delete restrict on update restrict;

