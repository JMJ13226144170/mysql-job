
use student;

select * from course;
select * from sc;
select * from student;
select * from teacher;

#吴孝涵

-- 1.查询"01"课程比"02"课程成绩高的 学生的信息 及课程分数

select * from sc a ,sc b ,student c where a.cid='01' and b.cid='02' and a.sid=b.sid and a.sid=c.sid and a.score>b.score
;
-- 1.1 查询存在" 01 "课程但可能不存在" 02 "课程的情况(不存在时显示为 null )

select * from sc a left join sc b on a.sid=b.sid and b.cid='02' where a.cid='01';

-- 1.2 查询同时存在01和02课程的情况

select * from sc a,sc b where a.cid='01' and b.cid='02' and A.sid=B.sid;

-- 1.3 查询选择了02课程但没有01课程的情况

select * from sc where cid='02' and sid not in (select sid from sc where cid='01');

-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩

select *,avg(score) from sc a inner join student b on a.sid=b.sid group by a.sid having avg(score)>=60;

-- 3.查询在 SC 表存在成绩的学生信息

select * from sc a inner join student b on a.sid=b.sid where score is not null group by a.sid;

-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的成绩总和

select *,count(*) 选课总数,sum(score) 成绩总和 from sc a inner join student b on a.sid=b.sid group by a.sid;

-- 5.查询「李」姓老师的数量

SELECT COUNT(*) from teacher WHERE tname LIKE '李%';

-- 6.查询学过「张三」老师授课的同学的信息

SELECT * from student where sid in 
(
	SELECT sid from sc where cid=
	(
		select cid from course where tid=
		(
			SELECT tid from teacher where tname='张三'
		)
	)
)
;

-- 7.查询没有学全所有课程的同学的信息

select * from student WHERE sid NOT in
(
	select sid  from sc
	GROUP BY sid
	HAVING count(*) >= 
	(
		SELECT COUNT(*) FROM course
	)
)
;
#殷晨旭

-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息

select * from student s
inner join sc on s.sid = sc.sid
where cid in
(
	select cid from sc
	where sid = '01'
)
and
s.sid <> '01'
group by s.sid;

-- 9.查询和" 01 "号的同学学习的课程完全相同的其他同学的信息

select * from student s
inner join sc on s.sid = sc.sid
where cid IN
(
	select cid from sc
	where sid = '01'
)
and s.sid not in
(
	select sid from sc
	left join 
	(
		select cid from sc
		where sid = '01'
	) a
	on sc.cid = a.cid
	where a.cid is null
	group by sc.sid
)
group by s.sid
having count(*) =
(
	select count(*) from sc
	where sid = 01
	group by sid
);

-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名

select * from student s
inner join sc on s.sid = sc.sid
where s.sid not in
(
	select s.sid from student s
	inner join sc on s.sid = sc.sid
	where cid = 02
	group by s.sid
select * from student s
inner join sc on s.sid = sc.sid
where s.sid not in
(
	select s.sid from student s
	inner join sc on s.sid = sc.sid
	where cid = 02
	group by s.sid
)
group by s.sid
)
group by s.sid;

-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩

select a.sid 学号,s.sname 姓名, count(*) 课程数量,avg(score) 平均成绩 from sc a inner join student s on a.sid=s.sid where a.score<60 group by a.sid having count(*)>=2;

-- 12.检索" 01 "课程分数小于 60，按分数降序排列的学生信息
##1.
select * from sc a inner join student s on a.sid=s.sid where cid='01' and score<60 ORDER BY score desc;

##2 无法排序
-- SELECT student.* from student where sid in
-- (
-- 	SELECT sid from sc
-- 	where cid='01' and score<60
-- )


-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩

#学生的平均成绩
-- 1
select a.score 成绩, c.*, b.cname 课程名称, avg(score) 平均成绩 from sc a right join course b on a.cid=b.cid right JOIN student c ON c.sid=a.sid group by a.sid,b.cname order by avg(score) desc;

-- 2
SELECT b.*,a.平均成绩 from 
(
	SELECT sid,ROUND(AVG(score),2)平均成绩 from sc
	GROUP BY sid
)a
right join 
(
	SELECT s.*,sc1.score 语文成绩,sc2.score 数学成绩,sc3.score 英语成绩 from student s
	LEFT join sc sc1 on sc1.sid=s.sid and sc1.cid='01'#语文
	LEFT join sc sc2 on sc2.sid=s.sid and sc2.cid='02'#数学
	LEFT join sc sc3 on sc3.sid=s.sid and sc3.cid='03'#英语
)b
on a.sid=b.sid
ORDER BY a.平均成绩 desc;

-- 14.查询各科成绩最高分、最低分和平均分,以如下形式显示：
-- 课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
-- 及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90
-- 要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序
-- 排列


-- 15.按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺



-- 15.1 按各科成绩进行行排序，并显示排名， Score 重复时合并名次


-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺





#陈宇翔






-- 17. 统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比


-- 18.查询各科成绩前三名的记录


-- 19.查询每门课程被选修的学生数


-- 20.查询出只选修两门课程的学生学号和姓名

-- 21. 查询男生、女生人数


-- 22. 查询名字中含有「风」字的学生信息


-- 23查询同名同性学生名单，并统计同名人数


-- 24.查询 1990 年出生的学生名单


-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列


-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩






#宋嘉伟




-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数

select * from sc a inner join student b on a.sid=b.sid where score <60 and cid in(select cid from course where cname='数学') group by a.sid;

-- 28. 查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）
-- 1
select * from student a left join sc b on a.sid=b.sid left join course c on b.cid=c.cid group by a.sid,c.cname;

-- 2
select student.sname,temp.score as 语文,temp1.score as 数学,temp2.score as 英语 from student
LEFT join 
(
select * from sc 
where cid = '01' 
) as temp
on temp.sid = student.sid 
LEFT join 
(
select * from sc 
where cid = '02' 
) as temp1
on temp1.sid = student.sid 
LEFT join 
(
select * from sc 
where cid = '03' 
) as temp2
on temp2.sid = student.sid;

-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数

select c.sname 姓名,b.cname 课程名称,a.score 分数 from sc a inner join course b on a.cid=b.cid inner join student c on a.sid=c.sid where score >70;

-- 30.查询不及格的课程

select * from sc where score<60;

-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名

select * from sc a inner join student b on a.sid=b.sid where cid='01' and score>=80;

-- 32.求每门课程的学生人数

select *,count(*) from sc a inner join student b on a.sid=b.sid group by a.cid;

-- 33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩


-- 34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生

-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩




-- 陈思锐

-- 36. 查询每门成绩最好的前两名

############################################################
-- 1____________________________________________________________________
select *,temp.cname 语文 from sc a inner join course b on a.cid=b.cid inner join (select * from course where cid='01') temp on a.cid=temp.cid order by a.score desc;
 -- 2_________________________________________________________________________
select * from 
(
	SELECT sc.*,IFNULL(t.名次,1) 名次 FROM sc 
	left join 
	(
		SELECT sc1.sid,sc1.cid ,sc1.score ,count(sc1.score)+1 名次 from  sc sc1
		inner join sc sc2 on sc1.cid =sc2.cid and sc1.sid!=sc2.sid and sc1.score<sc2.score
		GROUP BY sc1.sid,sc1.cid,sc1.score 
		order by sc1.score desc
	)
	t
	on t.sid=sc.sid and t.cid=sc.cid
	order by sc.cid asc, 名次 asc
)temp
where 名次<=2;


#############################################################

set names utf8;

select
	b.cid,b.sid,b.score,(count(a.score)) `rank` -- 统计a表成绩比b表成绩高的人数，结果+1就是排名
from
	sc a
right join
	sc b 
on
	a.cid = b.cid and a.sid <> b.sid and a.score > b.score -- 自连接，连接条件：课程id相等且学生id不想等，最后筛选出a表成绩比b表成绩高的列
GROUP BY
	b.cid, b.sid,b.score
having
	`rank` in (1,2) 
order by
	b.cid,b.sid

-- 1
-- 成绩排名表
select
	b.cid,b.sid,b.score,(count(a.score) + 1) `rank` -- 统计a表成绩比b表成绩高的人数，结果+1就是排名
from
	sc a
right join
	sc b 
on
	a.cid = b.cid and a.sid <> b.sid and a.score > b.score -- 自连接，连接条件：课程id相等且学生id不想等，最后筛选出a表成绩比b表成绩高的列
GROUP BY
	b.cid, b.sid,b.score
having
	`rank` in (1,2) 
order by
	b.cid,`rank`;

-- 结果表
select
	a.sname,b.*,c.cname
from
	student a
join
	(
		select
			b.cid,b.sid,b.score,(count(a.score) + 1) `rank`
		from
			sc a
		right join
			sc b
		on
			a.cid = b.cid and a.sid <> b.sid and a.score > b.score
		GROUP BY
			b.cid, b.sid,b.score
		having
			`rank` in (1,2)
	) b
on
	a.sid = b.sid
join
	course c
on
	b.cid = c.cid
order by
	b.cid,b.`rank`;
	
-- 2
-- 成绩排名表
select
	a.*,
	if(@cid = a.cid,@rownum:=@rownum+1,@rownum:=1) `rank`, -- 检查：当本行班级id与变量中保存的班级id相等时保持自增，不相等时重新从1开始计算
	@cid:=a.cid
from
	(select * from sc order by cid, score desc) a ,
	(select @rownum := 0,@cid:='') b; -- 设置变量
	
-- 结果表
select
	a.sname,t.sid,t.cid,t.score,t.rank,c.cname
from
	student a
join
	(
		select
			a.*,
			if(@cid = a.cid,@rownum:=@rownum+1,@rownum:=1) `rank`,
			@cid:=a.cid
		from
			(select * from sc order by cid, score desc) a ,
			(select @rownum := 0,@cid:='') b
	) t
on 
	a.sid = t.sid
join
	course c
on
	t.cid = c.cid
where
	t.`rank` in (1,2)
ORDER BY
	t.cid,t.`rank`;
	
	
-- 37. 统计每门课程的学生选修人数（超过 5 人的课程才统计）
select
	a.*,b.cname
from
	(
		select
			COUNT(distinct sid) s_num,cid
		from
			sc
		group by
			cid
		having
			s_num > 5
	) a
join
	course b
on
	b.cid = a.cid;

-- 38.检索至少选修两门课程的学生学号
select
	a.*,b.sname
from
	(
		select
			COUNT(distinct cid) c_num,sid
		from
			sc
		group by
			sid
		having
			c_num >= 2
	) a
join
	student b
on
	b.sid = a.sid

-- 39.查询选修了全部课程的学生信息
select
	a.*,b.*
from
	(
		select
			COUNT(distinct cid) c_num,sid
		from
			sc
		group by
			sid
		having
			c_num = (select count(*) from course)
	) a
join
	student b
on
	b.sid = a.sid;

-- 40.查询各学生的年龄，只按年份来算
-- 1
select
	*,year(now()) - year(sage) `年龄`
from
	student;

-- 41. 按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一
-- 1
select
	*,timestampdiff(year,sage,now()) `年龄`
from
	student;

-- 2
select
	a.sid,a.sname,a.sage,a.ssex,a.`年龄`
from
	(
		select
			*,
			@age := year(now()) - year(sage),
			case
				when month(now()) < month(sage) then @age := @age - 1
				when month(now()) = month(sage) then if(day(now()) < day(sage), @age := @age - 1,@age := @age)
				else @age
			END `年龄`
		from
			student
	) a;

-- 42.查询本周过生日的学生
-- 1 这种方法没办法考虑平年闰年
select *, week(sage) from student;
select week(now());

-- 结果表
select
	*, week(sage)
from
	student
where
	week(sage) = week(now());


-- 2这种方法考虑到平年闰年
select date_format(DATE_SUB(now(),INTERVAL (DAYOFWEEK(now()) - 2) day),'%Y-%m-%d');  -- 算出本周星期一的时间
select date_format(DATE_ADD(DATE_sub(now(),INTERVAL (DAYOFWEEK(now()) - 2) day),INTERVAL 6 day),'%Y-%m-%d'); -- 算出本周星期日的时间

-- 结果表
select
	*,DATE_ADD(sage,INTERVAL (year(now()) - year(sage)) year) `今年生日`
from
	student
where
	date_format(DATE_ADD(sage,INTERVAL (year(now()) - year(sage)) year),'%Y-%m-%d') BETWEEN
	date_format(DATE_SUB(now(),INTERVAL (DAYOFWEEK(now()) - 2) day),'%Y-%m-%d') and
	date_format(DATE_ADD(DATE_sub(now(),INTERVAL (DAYOFWEEK(now()) - 2) day),INTERVAL 6 day),'%Y-%m-%d') ;
	

-- 43. 查询下周过生日的学生
select
	*, week(sage)
from
	student
where
	week(sage) = week(now()) + 1;

-- 44.查询本月过生日的学生
select *, month(sage) from student;
select month(now());

select
	*, month(sage)
from
	student
where
	month(sage) = month(now());

-- 45.查询下月过生日的学生
select
	*, month(sage)
from
	student
where
	month(sage) = month(now()) + 1;