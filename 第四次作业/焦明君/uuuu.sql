
select * from course;
select * from sc;
select * from student;
select * from teacher;

-- 1.查询"01"课程比"02"课程成绩高的 学生的信息 及课程分数

select * from sc a,sc b,student c 
where a.cid='01' and b.cid='02' and a.sid=c.sid and b.sid=c.sid and a.score>b.score;

-- 1.1 查询存在" 01 "课程但可能不存在" 02 "课程的情况(不存在时显示为 null )

select * from sc a left join sc b on a.sid=b.sid and b.cid='02'
 where a.cid='01';

-- 1.2 查询同时存在01和02课程的情况
 
select * from sc a inner join sc b on a.sid=b.sid where a.cid='01' and b.cid='02';

-- 1.3 查询选择了02课程但没有01课程的情况

select * from sc where sid not in
(select sid from sc where cid ='01') and cid='02';

-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩

select A.sid,COUNT(A.sid),SUM(A.score),(SUM(A.score)/COUNT(A.sid)),B.* from sc A
INNER join student B on A.sid=B.sid
GROUP BY A.sid having (SUM(score)/COUNT(A.sid))>60;

-- 3.查询在 SC 表存在成绩的学生信息

select * from student where sid in
(
	SELECT DISTINCT sid  from sc
);

-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的成绩总和
-- 1
SELECT sid,sname,
(
	SELECT count(*) from sc where sid=student.sid
)选课总数,
(
	SELECT IFNULL(sum(score),0) from sc where sid=student.sid
)成绩总和
from student;
-- 2
select student.sid,student.sname,IFNULL(temp.选课总数,0)选课总数,IFNULL(temp.成绩总和,0)成绩总和 from student
LEFT join 
(
	SELECT sid,count(*)选课总数,sum(score)成绩总和 from sc 
	GROUP BY sid
)temp on temp.sid=student.sid;

-- 5.查询「李」姓老师的数量

SELECT COUNT(*) from teacher WHERE tname LIKE '李%';

-- 6.查询学过「张三」老师授课的同学的信息

SELECT * from student where sid in 
(
	SELECT sid from sc where cid=
	(
		select cid from course where tid=
		(
			SELECT tid from teacher where tname='张三'
		)
	)
);

-- 7.查询没有学全所有课程的同学的信息

select * from student WHERE sid NOT in
(
	select sid  from sc
	GROUP BY sid
	HAVING count(*) >= 
	(
		SELECT COUNT(*) FROM course
	)
);

-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息

select * from student s
inner join sc on s.sid = sc.sid
where cid in
(
	select cid from sc
	where sid = '01'
)
and
s.sid <> '01'
group by s.sid;

-- 9.查询和" 01 "号的同学学习的课程完全相同的其他同学的信息

select * from student s
inner join sc on s.sid = sc.sid
where cid IN
(
	select cid from sc
	where sid = '01'
)
and s.sid not in
(
	select sid from sc
	left join 
	(
		select cid from sc
		where sid = '01'
	) a
	on sc.cid = a.cid
	where a.cid is null
	group by sc.sid
)
group by s.sid
having count(*) =
(
	select count(*) from sc
	where sid = 01
	group by sid
);

-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名

select * from student s
inner join sc on s.sid = sc.sid
where s.sid not in
(
	select s.sid from student s
	inner join sc on s.sid = sc.sid
	where cid = 02
	group by s.sid
)
group by s.sid;
