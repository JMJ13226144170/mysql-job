-- select 
-- 1.查询所有用户信息以及卡数量

select *,(select count(*) from bankcard where AccountId=accountinfo.AccountId group by AccountId) 卡数量 from accountinfo;

-- from 
-- 2.查询所有用户信息以及卡数量和总余额

select accountinfo.*,b.卡数量,b.总余额 from 
(select bankcard.AccountId,count(*) 卡数量,sum(cardmoney) 总余额 from bankcard group by AccountId) b 
inner join accountinfo on accountinfo.AccountId=b.AccountId;

-- where 
-- 3.查询余额最高的账户
-- 总余额

select * from accountinfo 
where AccountId=
(select AccountId from
 (select *,sum(cardmoney) from bankcard group by bankcard.AccountId order by sum(cardmoney) desc limit 1) b);
 
-- 单卡余额

SELECT * from accountinfo where AccountId=(select AccountId from (select *,MAX(cardmoney) from bankcard
) b);


-- 4.查询出其他储户余额比关羽某张(6225098234235)银行卡多的银行卡号，显示卡号，身份证，姓名，余额

select * from accountinfo INNER JOIN ( select * from bankcard where cardmoney>(select cardmoney from bankcard where cardno='6225098234235'
) ) b on accountinfo.AccountId=b.AccountId;

-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）

select * from cardexchange where cardid in (select cardid from bankcard WHERE cardmoney>=(select max(cardmoney) from bankcard)
);


-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

SELECT b.cardno,a.AccountCode,a.RealName,b.CardMoney  FROM accountinfo a,
(select * from bankcard where cardid in(select cardid from cardexchange where MoneyOutBank>'0') ) b where a.AccountId=b.AccountId;

-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

select b.cardno,a.AccountCode,a.RealName,b.CardMoney from accountinfo a,
(select * from bankcard where cardid in (select cardid from cardexchange where moneyoutbank='0')) b where a.AccountId=b.AccountId;


-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额

select b.cardno,a.AccountCode,a.RealName,b.CardMoney from accountinfo a,
(select * from bankcard where cardid not in(select cardidout from cardtransfer ) and cardid not in (select cardidin from cardtransfer )) b 
where a.AccountId=b.AccountId;

-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称

select a.RealName,b.* from accountinfo a inner join (
(select * from bankcard where CardMoney>'100'))b on a.AccountId=b.AccountId;


-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数
select bankcard.Cardno 卡号,accountinfo.AccountCode 身份证,accountinfo.RealName 姓名,bankcard.CardMoney 余额,count(cardexchange.CardId) 交易次数 from bankcard,accountinfo,cardexchange 
where bankcard.AccountId=accountinfo.AccountId and bankcard.CardId=cardexchange.CardId group by cardexchange.CardId HAVING count(cardexchange.CardId)=
(select count(cardid) from cardexchange group by cardid ORDER BY count(cardid) desc limit 1)

