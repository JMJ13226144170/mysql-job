/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 17:29:20                            */
/*==============================================================*/


drop table if exists department;

drop table if exists diagnose;

drop table if exists expert;

drop table if exists "order";

drop table if exists patient;

/*==============================================================*/
/* Table: department                                            */
/*==============================================================*/
create table department
(
   id                   int not null,
   name                 varchar(15) not null comment '名称',
   expert_id            int not null comment '专家ID',
   department_category  varchar(15) not null comment '科室类别',
   primary key (id)
);

alter table department comment '科室表';

/*==============================================================*/
/* Table: diagnose                                              */
/*==============================================================*/
create table diagnose
(
   id                   int not null,
   diagnose_time        datetime not null comment '出诊时间',
   expert_id            int not null comment '专家ID',
   patient_id           int not null comment '患者ID',
   primary key (id)
);

alter table diagnose comment '出诊表';

/*==============================================================*/
/* Table: expert                                                */
/*==============================================================*/
create table expert
(
   id                   int not null,
   name                 varchar(15) not null comment '姓名',
   photo                varchar(99) not null comment '头像照片',
   sex                  char(1) not null comment '性别',
   department_category  varchar(15) not null comment '科室类别',
   department           varchar(15) not null comment '科室',
   job                  varchar(15) not null comment '职称',
   speciality           varchar(66) comment '特长',
   diagnose_time        datetime not null comment '出诊时间',
   primary key (id)
);

alter table expert comment '专家表';

/*==============================================================*/
/* Table: "order"                                               */
/*==============================================================*/
create table "order"
(
   id                   int not null,
   date                 date not null comment '日期',
   time                 time not null comment '时间',
   patient_id           int not null comment '患者ID',
   primary key (id)
);

alter table "order" comment '预约时间表';

/*==============================================================*/
/* Table: patient                                               */
/*==============================================================*/
create table patient
(
   id                   int not null,
   name                 varchar(13) not null comment '姓名',
   sex                  char(1) not null comment '性别',
   age                  int comment '年龄',
   primary key (id)
);

alter table patient comment '患者表';

alter table department add constraint FK_Reference_3 foreign key (expert_id)
      references expert (id) on delete restrict on update restrict;

alter table diagnose add constraint FK_Reference_1 foreign key (expert_id)
      references expert (id) on delete restrict on update restrict;

alter table diagnose add constraint FK_Reference_2 foreign key (patient_id)
      references patient (id) on delete restrict on update restrict;

alter table "order" add constraint FK_Reference_4 foreign key (patient_id)
      references patient (id) on delete restrict on update restrict;

